import { Component } from '@angular/core';
import { NavController, ItemSliding } from 'ionic-angular';

@Component({
  selector: 'tasklist',
  templateUrl: 'tasklist.html'
})
export class Tasklist {

  tasks:Array<any> = [];

  constructor(public navCtrl: NavController) {
    this.tasks = [
      {title:'Nasi Goreng', status:'open'},
      {title:'Nasi Padang', status:'open'},
      {title:'Nasi Kuning', status:'open'},
      {title:'Nasi Jagung', status:'open'},
      {title:'Nasi Sop', status:'open'}
    ]
  }

  //fungsi menambah task
  addItem() {
    let theNewTask: string = prompt("New Task");
    if(theNewTask!='') {
      this.tasks.push({title: theNewTask, status:'open'})
    }
  }
  //funngsi mengedit task
  markAsDone(slidingItem: ItemSliding, task:any) {
     task.status = 'done';
     slidingItem.close();
  }
  //fungsi menghapus task
  removeTask(slidingItem: ItemSliding, task:any){
    task.status= 'removed';
    let index = this.tasks.indexOf(task);
    if (index > -1){
      //prompt("Yakin ingin menghapus");
      this.tasks.splice(index,1);
    }
    slidingItem.close();
  }

}
